require 'gitlab'
PER_PAGE = 10
PROJECT_ID = 13083 #gitlab_ce

Gitlab.configure do |config|
    config.endpoint = 'https://gitlab.com/api/v4'
    config.private_token = ENV['PAT']

end


begin
    text = File.open('files_dirs.csv')
    authorlist = File.open('authors.csv', 'w')
    i=0
    ids = Hash.new
    text.each_line do |line|
        i+=1
        values = line.split('|')
        id = values[0]
        unless  ids.key?(id.to_s)
            mr = Gitlab.merge_request(PROJECT_ID, id)
            authorlist.puts mr.author.username
            ids[id.to_s] = id
        end
        #puts values[0]
        puts i
    end
ensure
    text.close
    authorlist.close
end