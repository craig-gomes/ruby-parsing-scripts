require 'json'
require 'net/http'

#https://gitlab.com/api/v4/projects/gitlab-org%2fgitlab-ce/issues?labels=database&page=2&per_page=100

PER_PAGE = 10

all_issues = []
should_continue = true

    (1..100).each do |current_page|

      if !should_continue
        break
      end

      url = "https://gitlab.com/api/v4/projects/13083/merge_requests?labels=database&page=#{current_page}&per_page=#{PER_PAGE}"

      response = Net::HTTP.get(URI(url))
      puts JSON.parse(response)
    break
    end
