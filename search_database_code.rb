require 'pry'
require 'gitlab'
PER_PAGE = 10
PROJECT_ID = 13083 #gitlab_ce

Gitlab.configure do |config|
    config.endpoint = 'https://gitlab.com/api/v4'
    config.private_token = ENV['PAT']

end

all_mrs = []
should_continue = true

    (1..100).each do |current_page|

      if !should_continue
        break
      end

        merge_requests = Gitlab.merge_requests(PROJECT_ID, {labels: "database",per_page:PER_PAGE, page:current_page})

        if merge_requests.size < PER_PAGE
            should_continue = false
        end

        merge_requests.each do |i|
            all_mrs << i
        end

        puts "current page = #{current_page} all_mrs length = #{all_mrs.length}"

        #remove me.... this is to limit the test
        #break
    end

    file = File.open("mr_changes.csv", "w")
begin
    file.puts 'mr.iid|old_path|new_path'    
    all_mrs.each do |mr|
        #puts "... #{mr.iid} #{mr.title} #{mr.web_url} : #{mr.state}"
        changes = Gitlab.merge_request_changes(PROJECT_ID, mr.iid)
        changes.changes.each do |change|
            #puts change["old_path"]
            file.puts "#{mr.iid}|#{change['old_path']}|#{change['new_path]']}"
        end 
    end
ensure
    file.close
end 