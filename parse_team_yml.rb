require "open-uri"
require "yaml"

url = 'http://about.gitlab.com/company/team/team.yml'

yaml_content = open(url){|f| f.read}
yaml_data = YAML::load(yaml_content)
file = File.open("team.csv", "w")
file.puts 'gitlab|role|start'
yaml_data.each do |slug|
    if slug['gitlab'] then
       file. puts "#{slug['gitlab']}|#{slug['role']}|#{slug['start_date']}"
    end
end
file.close
puts 'done'