begin
    text = File.open('mr_changes.csv')
    dirlist = File.open('files_dirs.csv', 'w')
    text.each_line do |line|
        values = line.split('|')
        dirlist.puts "#{values[0]}|#{values[1]}|#{File.dirname(values[1])}" 
    end
ensure
    text.close
    dirlist.close
end