require 'pry'
require 'gitlab'
require 'pp'
require 'dotenv/load'

PER_PAGE = 100
#PROJECT_ID = 13083 #gitlab_ce
PROJECT_ID = 9970
LABELS = "group::memory"

#TODO - Make this commandline so that you don't have to hard code epic IDs
EPICS = Array[1855, 1810]


Gitlab.configure do |config|
    config.endpoint = 'https://gitlab.com/api/v4'
    #puts 'Enter your private token'
    config.private_token = ENV["PAT"]
end
$all_mrs = []

class Gitlab::Client
    module Epics
  
      def epics(team)
        get("/groups/9970/epics?labels=#{team}", {per_page:PER_PAGE})
      end
  
      def epic_children(epic_iid)
        get("/groups/9970/epics/#{epic_iid}/epics", {per_page:PER_PAGE})
      end
  
      def epic_issues(epic_iid)
        get("/groups/9970/epics/#{epic_iid}/issues", {per_page:PER_PAGE})
      end
  
    end

    module MergeRequests
      def related(project_id, issue_id)
        get("/projects/#{project_id}/issues/#{issue_id}/related_merge_requests", {per_page:PER_PAGE})
      end
    end
  end
  
  module Gitlab
    class Client < API
      include Epics
      include MergeRequests
    end
  end
 
f_issues = File.open("issues.csv", "w")
f_issues.puts "ISSUE_IID|ISSUE_TITLE|AUTHOR_ID|MASKED_LABELS|ISSUE_CREATED_AT|ISSUE_CLOSED_AT"

def get_issue_mrs(pid, issue_id)
  #puts "get_issue_mrs #{pid}-#{issue_id}"
  merge_requests = Gitlab.related(pid.to_i, issue_id.to_i)
  
  merge_requests.each do |mr|
    $all_mrs << mr
  end

end

EPICS.each do |epic|
    should_continue = true

    while should_continue do

        
        issues = Gitlab.epic_issues(epic)

        if issues.size < PER_PAGE 
        should_continue = false
        end 

        issues.each do |issue|
            begin
                f_issues.puts "#{issue.iid}|#{issue.title}|#{issue.author.id}|#{issue.labels.to_s}|#{issue.created_at}|#{issue.closed_at}"
                #puts "Issue size #{issues.size} should continue #{should_continue}"
               get_issue_mrs(issue.project_id, issue.iid)
            rescue StandardError => e  
                puts "StandardError raised #{e.message} #{e.backtrace.inspect}"  
                #puts e.backtrace.inspect  
            rescue
                puts "someting bad happened with #{issue.title}"
            end
        end 
    end
end

f_mrs = File.open("mrs.csv", "w")
f_mrs.puts "MR_IID|TITLE|AUTHOR_ID|LABELS|CREATED_AT|CLOSED|AT"
$all_mrs.each do |mr|
  f_mrs.puts "#{mr.iid}|#{mr.title}|#{mr.author.id}|#{mr.labels}|#{mr.created_at}|#{mr.merged_at}"
end

