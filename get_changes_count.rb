# sample URL https://gitlab.com/api/v4/projects/gitlab-org%2fomnibus-gitlab/merge_requests/1783

# Steps
# Read file
# Assemble URL to retrieve data
# get MR ID
# Get project and URL Encode
# Parse to JSON
# Get changes_count for project and MR
# Output to CSV for now

require 'uri'
require 'net/http'
require 'json'

def parse_json(url)
    
    uri = URI(url)
    response = Net::HTTP.get(uri)
    json = JSON.parse(response)

    json['changes_count']
end

file = File.open("changes_count.csv", "w")
begin
    starting = Process.clock_gettime(Process::CLOCK_MONOTONIC)
    file.puts 'project|mr|changes_count|url'
    i=0
    File.readlines('average_issuables_merged_per_month.csv').drop(1).each do |line|
        values = line.split(',')
        url =  ("https://gitlab.com/api/v4/projects/#{values[2].gsub('/', '%2f')}/merge_requests/#{values[1]}")
        file.puts "#{values[2]}|#{values[1]}|#{parse_json(url)}|#{url}"
        ending = Process.clock_gettime(Process::CLOCK_MONOTONIC)
        puts (i+=1).to_s + " elapsed time: " + (ending - starting).to_s
    end
rescue StandardError => err
    puts "Exception: " + err
ensure
    file.close unless file.nil?
end

